<?php

namespace Drupal\integration_chargebee\Controller;

use Drupal\Core\Render\Markup;
use ChargeBee\ChargeBee\Environment;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\integration_chargebee\Services\ChargebeeService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * User controller for integration chargebee.
 */
class UserController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The integration chargebee service.
   *
   * @var \Drupal\integration_chargebee\Services\ChargebeeService
   */
  protected $integrationChargebeeservice;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The string translation.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $renderer;

  /**
   * Constructs a new Drupal\integration_chargebee\Controller\UserController.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\integration_chargebee\Services\ChargebeeService $integrationChargebeeservice
   *   The integration chargebee service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    AccountInterface $account,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    Messenger $messenger,
    ChargebeeService $integrationChargebeeservice,
    LoggerChannelFactoryInterface $loggerFactory,
    ConfigFactory $configFactory,
    TranslationInterface $stringTranslation,
    RendererInterface $renderer) {
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->integrationChargebeeservice = $integrationChargebeeservice;
    $this->loggerFactory = $loggerFactory->get('integration_chargebee');
    $this->configFactory = $configFactory;
    $this->stringTranslation = $stringTranslation;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('messenger'),
      $container->get('integration_chargebee.integration_chargebee_service'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('string_translation'),
      $container->get('renderer'),
    );
  }

  /**
   * Returns a payement success data.
   */
  public function subscription() {

    $rows = [];
    $config = $this->configFactory->getEditable('integration_chargebee.settings');
    $site_name = $config->get('integration_chargebee_api_site_name');
    $site_api_key = $config->get('integration_chargebee_api_key');
    // $site_plan = $config->get('integration_chargebee_api_plan');
    Environment::configure("$site_name", "$site_api_key");
    $user = $this->entityTypeManager->getStorage('user')->load($this->account->id());
    $user_id = $user->id();
    $query = $this->connection->select('integration_chargebee_subscription', 'n');
    $query->fields('n', [
      'uid',
      'subscription_id',
      'plan_id',
      'status',
      'current_term_start',
      'current_term_end',
      'amount',
    ]);

    $query->condition('n.uid', $user_id);
    $all_doamin = $query->execute()->fetchAll();
    if (!empty($all_doamin)) {
      $link = Markup::create('<a href="javascript:void(0)" data-cb-type="portal">Manage Subscription</a>');
      foreach ($all_doamin as $kays => $values) {
        if ($values->subscription_id != '') {
          $rows[] = [
            'plan_id' => $values->plan_id,
            'subscription_id' => $values->subscription_id,
            'start_date' => date('m/d/Y', $values->current_term_start),
            'end_date' => date('m/d/Y', $values->current_term_end),
            'amount' => $values->amount,
            'status' => $values->status,
            'link' => $link
            ,
          ];
        }
      }
    }

    $header = [
      'plan_id' => $this->t('Plan Id'),
      'subscription_id' => $this->t('Subscription Id'),
      'start_date' => $this->t('Start Date'),
      'end_date' => $this->t('End Date'),
      'amount' => $this->t('Amount'),
      'status' => $this->t('Status'),
      'link' => $this->t('link'),
    ];

    $build['dblog_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'id' => 'admin-dblog',
        'class' => [
          'admin-dblog',
        ],
      ],
      '#empty' => $this->t('You have not published any site yet.'),
      '#attached' => [
        'library' => [
          'dblog/drupal.dblog',
        ],
      ],
    ];
    $build['dblog_pager'] = [
      '#type' => 'pager',
    ];
    $build['#cache']['max-age'] = 0;

    return [
      '#type' => '#markup',
      '#markup' => $this->renderer->render($build),

    ];

  }

}
