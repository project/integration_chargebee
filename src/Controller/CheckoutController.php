<?php

namespace Drupal\integration_chargebee\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The checkout controller for the integration chargebee module.
 */
class CheckoutController extends ControllerBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $renderer;

  /**
   * Construct a new Drupal\integration_chargebee\Controller\CheckoutController object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(ConfigFactory $configFactory, RendererInterface $renderer) {
    $this->configFactory = $configFactory;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('renderer'),

    );
  }

  /**
   * Returns a Plan data.
   */
  public function plan() {

    $plan_config = $this->configFactory->getEditable('integration_chargebee.plan');
    $config = $this->configFactory->getEditable('integration_chargebee.settings');
    $plan = $plan_config->get();
    $items = [];
    foreach ($plan as $key => $value) {

      $items[] = [
        'status' => $config->get('integration_chargebee_api_' . $value . '_status'),
        'name' => $config->get('integration_chargebee_api_' . $value . '_title'),
        'id' => $config->get('integration_chargebee_api_' . $value),
        'amount' => $config->get('integration_chargebee_api_' . $value . '_amount'),
        'description' => $config->get('integration_chargebee_api_' . $value . '_details'),
      ];
    }
    $build['page'] = [
      '#theme' => 'plan_list',
      '#items' => $items,
    ];
    $build['#cache']['max-age'] = 0;
    $build['#attached']['library'][] = 'integration_chargebee/integration_chargebee_style';
    return [
      '#type' => '#markup',
      '#markup' => $this->renderer->render($build),

    ];

  }

}
