<?php

namespace Drupal\integration_chargebee\Controller;

use Drupal\Core\Url;
use ChargeBee\ChargeBee\Environment;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Controller\ControllerBase;
use ChargeBee\ChargeBee\Models\Subscription;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\integration_chargebee\Services\ChargebeeService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Payment controller for integration chargebee.
 */
class PaymentController extends ControllerBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The integration chargebee service.
   *
   * @var \Drupal\integration_chargebee\Services\ChargebeeService
   */
  protected $integrationChargebeeservice;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new Drupal\integration_chargebee\Controller\PaymentController.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\integration_chargebee\Services\ChargebeeService $chargebeeService
   *   The integration chargebee service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request service.
   */
  public function __construct(
    AccountInterface $account,
    Connection $connection,
    Messenger $messenger,
    ChargebeeService $chargebeeService,
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactoryInterface $config_factory,
    RequestStack $requestStack) {
    $this->account = $account;
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->integrationChargebeeservice = $chargebeeService;
    $this->loggerFactory = $logger_factory->get('integration_chargebee');
    $this->configFactory = $config_factory;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('database'),
      $container->get('messenger'),
      $container->get('integration_chargebee.integration_chargebee_service'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('request_stack'),
    );
  }

  /**
   * Returns a payement success data.
   */
  public function success() {
    $config = $this->configFactory->get('integration_chargebee.settings');
    $site_name = $config->get('integration_chargebee_api_site_name');
    $site_api_key = $config->get('integration_chargebee_api_key');
    Environment::configure("$site_name", "$site_api_key");
    $sub_id = $this->requestStack->getCurrentRequest()->query->get('sub_id');
    if (isset($sub_id)) {
      try {
        $result = Subscription::retrieve($sub_id);
        if (isset($result)) {
          $subscription = $result->subscription();

          $customer = $result->customer();
          $subscriptions = $this->integrationChargebeeservice->getProtectedValue($subscription, '_data');
          $subscriptions_item = $this->integrationChargebeeservice->getProtectedValue($subscriptions['subscriptionItems'][0], '_data');
          $startedAt = $subscriptions['startedAt'];
          $nextBillingAt = $subscriptions['nextBillingAt'];
          $amount = $subscriptions_item['amount'] / 100;

          $this->connection->insert('integration_chargebee_subscription')
            ->fields([
              'uid' => $this->account->id(),
              'subscription_id' => $sub_id,
              'plan_id' => $subscriptions_item['itemPriceId'],
              'status' => 'Active',
              'current_term_start' => $startedAt,

              'current_term_end' => $nextBillingAt,
              'amount' => $amount,
            ])
            ->execute();
          $this->connection->insert('integration_chargebee_customer')
            ->fields([
              'uid' => $this->account->id(),
              'customer_id' => $customer->id,

            ])

            ->execute();

        }
      }
      catch (Exception $e) {

      }
    }
    $build = [
      '#markup' => $this->t('Payment Successfully completed.'),
    ];
    $build['#cache']['max-age'] = 0;
    $this->messenger->addMessage('Payment Successfully completed.');

    $url = Url::fromUri('internal:/user/' . $this->account->id() . '/subscriptions');
    $response = new RedirectResponse($url->toString());
    $response->send();

    return $build;
  }

}
