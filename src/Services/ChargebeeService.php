<?php

namespace Drupal\integration_chargebee\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * ChargebeeService Service class.
 */
class ChargebeeService {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a new ChargebeeService object.
   *
   * @param Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   */
  public function __construct(AccountInterface $currentUser,
  EntityTypeManagerInterface $entity_type_manager,
  Connection $connection,
  MessengerInterface $messenger,
  LoggerChannelFactoryInterface $loggerFactory,
  ) {
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->loggerFactory = $loggerFactory;

  }

  /**
   * Implement the getProtectedValue.
   */
  public function getProtectedValue($obj, $name) {
    $array = (array) $obj;
    $prefix = chr(0) . '*' . chr(0);
    return $array[$prefix . $name];
  }

  /**
   * Implements the getSubscriptionDetails().
   */
  public function getSubscriptionDetails($subscription_id) {
    $query = $this->connection->select('integration_chargebee_subscription', 'n');
    $query->fields('n');
    $query->condition('subscription_id', $subscription_id);
    $subscription_details = $query->execute()->fetchAssoc();
    if (!empty($subscription_details)) {
      return $subscription_details;
    }
    else {
      return false;
    }

  }

  /**
   * Implements to get the customer ID.
   */
  public function getCustomerId() {
    $query = $this->connection->select('integration_chargebee_customer', 'n');
    $query->fields('n',['customer_id']);
    $query->condition('uid', $this->currentUser->id());
    $customer_id = $query->execute()->fetchAssoc();
    if (!empty($customer_id)) {
      return $customer_id;
    }
    else {
      return false;
    }
  }

  /**
   * Implements to get the current subscribed plan.
   */
  public function getPlan() {
    $query = $this->connection->select('integration_chargebee_subscription', 'n');
    $query->fields('n',['plan_id']);
    $query->condition('uid', $this->currentUser->id());
    $plan_id = $query->execute()->fetchAssoc();
    if (!empty($plan_id)) {
      return $plan_id;
    }
    else {
      return false;
    }
  }

  /**
   * Implements to get subscription status.
   */
  public function getSubscriptionStatus() {
    $query = $this->connection->select('integration_chargebee_subscription', 'n');
    $query->fields('n',['status']);
    $query->condition('uid', $this->currentUser->id());
    $status = $query->execute()->fetchAssoc();
    if (!empty($status)) {
      return $status;
    }
    else {
      return false;
    }
  }

  /**
   * Implements to get the subscription ID.
   */
  public function getSubscriptionId() {
    $query = $this->connection->select('integration_chargebee_subscription', 'n');
    $query->fields('n',['subscription_id']);
    $query->condition('uid', $this->currentUser->id());
    $subscription_id = $query->execute()->fetchAssoc();
    if (!empty($subscription_id)) {
      return $subscription_id;
    }
    else {
      return false;
    }
  }

  /**
   * Implements function to get data by field name.
   */

}
