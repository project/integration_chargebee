<?php

namespace Drupal\integration_chargebee\Plugin\rest\resource;

use Psr\Log\LoggerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\integration_chargebee\Services\ChargebeeService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "subscription_details_resource",
 *   label = @Translation("Chargebee Suscription Details Rest Resource"),
 *   uri_paths = {
 *     "canonical" = "/v1/api/get-subscription-details"
 *   }
 * )
 */
class SubscriptionDetailsResource extends ResourceBase {

  /**
   * Constructs object for SubscriptionDetailsResource.
   */
  public function __construct(array $config, $module_id, $module_definition, array $serializer_formats, LoggerInterface $logger, RequestStack $request_stack, ChargebeeService $integration_chargebee_service) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->requestStack = $request_stack;
    $this->integration_chargebeeService = $integration_chargebee_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('domain_controller_rest_resource'),
      $container->get('request_stack'),
      $container->get('integration_chargebee.integration_chargebee_service')
    );
  }

  /**
   * Get function.
   */
  public function get() {
    if ($this->requestStack->getCurrentRequest()->query->has('subscription_id')) {
      // $site_id = $this->requestStack->getCurrentRequest()->query->get('subscription_id');
      $subscription_details = $this->integration_chargebeeService->getSubscriptionDetails($subscription_id);
      $success_msg_code = 200;
      // $success_msg = json_encode($subscription_details);
      $response = [
        'message_code' => $success_msg_code,
        'message' => $subscription_details,
      ];
    }
    else {
      $error_msg_code = 404;
      $error_msg = 'Site domain id not provided.';
      $response = ['message_code' => $error_msg_code, 'message' => $error_msg];
    }
    return new ModifiedResourceResponse($response);
  }

}
