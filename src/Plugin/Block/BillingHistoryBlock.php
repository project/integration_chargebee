<?php

namespace Drupal\integration_chargebee\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use ChargeBee\ChargeBee\Environment;
use ChargeBee\ChargeBee\Models\Transaction;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provide a block with integration_chargebee Billing history subscription wise.
 *
 * @Block(
 * id = "integration_chargebee_billing_history",
 * admin_label = @Translation("integration_chargebee Billing History")
 * )
 */
class BillingHistoryBlock extends BlockBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The integration chargebee service.
   *
   * @var \Drupal\integration_chargebee\Services\ChargebeeService
   */
  protected $integrationChargebeeservice;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * The request stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new BillingHistoryBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\integration_chargebee\Services\ChargebeeService $chargebeeService
   *   The integration chargebee service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request service.
   */
  public function __construct(array $configuration,
  $plugin_id,
  $plugin_definition,
  integration_chargebeeService $chargebeeService,
  ConfigFactoryInterface $config_factory,
  RequestStack $requestStack
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->integrationChargebeeservice = $chargebeeService;
    $this->configFactory = $config_factory;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('integration_chargebee.integration_chargebee_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $rows = [];

    $config = $this->configFactory->get('integration_chargebee.settings');
    $site_name = $config->get('integration_chargebee_api_site_name');
    $site_api_key = $config->get('integration_chargebee_api_key');
    Environment::configure("$site_name", "$site_api_key");
    $subscription_id = $this->requestStack->getCurrentRequest()->query->get('subscriptionId');
    if (isset($subscription_id)) {
      $all = Transaction::all([
        "status[is]" => "success",
        "sortBy[desc]" => "date",
        "limit" => '30',
        "subscriptionId[is]" => $subscription_id,
      ]);
      foreach ($all as $entry) {

        $transaction = $entry->transaction();
        $transactions = $this->integrationChargebeeservice->getProtectedValue($transaction, '_data');
        $rows[] = [
          'id' => $entry->transaction()->id,
          'subscription id' => $transactions['subscriptionId'],
          'customer_id' => $transactions['customerId'],
          'Occurred_On' => date('m/d/Y', $transactions['date']),
          'type' => $transactions['type'],
          'gateway' => $transactions['gateway'],
          'payment_method' => $transactions['maskedCardNumber'],
          'amount' => '$' . $transactions['amount'] / 100,
          'status' => $transactions['status'],
        ];
      }
    }
    $header = [
      'id' => $this->t('ID'),
      'subscription' => $this->t('Subscription Id'),
      'customer_id' => $this->t('Customer Id'),
      'Occurred_On' => $this->t('Occurred On'),
      'type' => $this->t('Type'),
      'gateway' => $this->t('Gateway'),
      'payment_method' => $this->t('Payment Method'),
      'amount' => $this->t('Amount'),
      'status' => $this->t('Status'),
    ];

    $build['dblog_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'id' => 'admin-dblog',
        'class' => [
          'admin-dblog',
        ],
      ],
      '#empty' => $this->t('You have not published any site yet.'),
      '#attached' => [
        'library' => [
          'dblog/drupal.dblog',
        ],
      ],
    ];

    $build['#cache']['max-age'] = 0;
    return $build;

  }

}
