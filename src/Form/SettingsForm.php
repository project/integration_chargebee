<?php

namespace Drupal\integration_chargebee\Form;

use ChargeBee\ChargeBee\Environment;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use ChargeBee\ChargeBee\Models\ItemPrice;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Builds the chargebee settings form.
 */
class SettingsForm extends ConfigFormBase {
  use StringTranslationTrait;
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The chargebee plans.
   *
   * @var array
   */
  protected $chargbeePlans = [];

  /**
   * Constructs a new Drupal\integration_chargebee\Form\SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->chargbeePlans = $this->configFactory->get('integration_chargebee.plan');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'integration_chargebee.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->configFactory->get('integration_chargebee.settings');
    $site_name = $config->get('integration_chargebee_api_site_name');
    $site_api_key = $config->get('integration_chargebee_api_key');

    Environment::configure("$site_name", "$site_api_key");

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('API Details'),
    ];
    $form['api']['integration_chargebee_api_site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name'),
      '#default_value' => $config->get('integration_chargebee_api_site_name'),
      '#disabled' => FALSE,
    ];
    $form['api']['integration_chargebee_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#default_value' => $config->get('integration_chargebee_api_key'),
      '#disabled' => FALSE,
    ];

    $form['api']['integration_chargebee_api_js_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use CDN version of Chargebee JS'),
      '#default_value' => $config->get('integration_chargebee_api_js_link'),
    ];

    $data = $this->chargbeePlans->get();
    if (!empty($data)) {

      foreach ($data as $keys => $value) {

        $result = ItemPrice::retrieve($value);
        $itemPrice = $result->itemPrice();

        $form['api'][$value] = [
          '#type' => 'details',
          '#title' => $itemPrice->itemId,
          '#weight' => 50,
          '#open' => FALSE,
        ];

        $form['api'][$value]['integration_chargebee_api_' . $value . '_title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('<span>@itemprice-name</span> Title', ['@itemprice-name' => $itemPrice->name]),
          '#default_value' => $config->get('integration_chargebee_api_' . $value . '_title') ? $config->get('integration_chargebee_api_' . $value . '_title') : $itemPrice->itemId,
        ];

        $form['api'][$value]['integration_chargebee_api_' . $value] = [
          '#type' => 'textfield',
          '#title' => $this->t('<span>@itemprice-name</span> ID', ['@itemprice-name' => $itemPrice->name]),
          "#disabled" => TRUE,
          '#default_value' => $config->get('integration_chargebee_api_' . $value) ? $config->get('integration_chargebee_api_' . $value) : $itemPrice->id,
        ];

        $form['api'][$value]['integration_chargebee_api_' . $value . '_amount'] = [
          '#type' => 'textfield',
          '#title' => $this->t('<span>@itemprice-name</span> Amount', ['@itemprice-name' => $itemPrice->name]),
          "#disabled" => TRUE,
          '#default_value' => $config->get('integration_chargebee_api_' . $value . '_amount') ? $config->get('integration_chargebee_api_' . $value . '_amount') : $itemPrice->price / 100,
        ];

        $form['api'][$value]['integration_chargebee_api_' . $value . '_details'] = [
          '#type' => 'text_format',
          '#format' => 'full_html',
          '#weight' => '20',
          '#title' => $this->t('<span>@itemprice-name</span> Details', ['@itemprice-name' => $itemPrice->name]),
          '#default_value' => $config->get('integration_chargebee_api_' . $value . '_details'),
        ];
        $form['api'][$value]['integration_chargebee_api_' . $value . '_status'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable/Disable.'),
          '#default_value' => $config->get('integration_chargebee_api_' . $value . '_status'),
        ];
      }
    }
    $form['api']['integration_chargebee_webhook_hash'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook hash'),
      '#default_value' => $config->get('integration_chargebee_webhook_hash'),
    ];
    $form['service'] = [
      '#type' => 'details',
      '#title' => $this->t('Test Connection'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['service']['verify'] = [
      '#type' => 'submit',
      '#value' => $this->t('Verify Connection'),
      '#submit' => ['integration_chargebee_service_verify_submit'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    if (!empty($this->chargbeePlans)) {
      $plan = $this->chargbeePlans->get();
      foreach ($plan as $key => $value) {
        $this->config('integration_chargebee.settings')
          ->set('integration_chargebee_api_' . $key, $form_state->getValue('integration_chargebee_api_' . $key))
          ->set('integration_chargebee_api_' . $key . '_amount', $form_state->getValue('integration_chargebee_api_' . $key . '_amount'))
          ->set('integration_chargebee_api_' . $key . '_details', $form_state->getValue('integration_chargebee_api_' . $key . '_details')['value'])
          ->set('integration_chargebee_api_' . $key . '_title', $form_state->getValue('integration_chargebee_api_' . $key . '_title'))
          ->set('integration_chargebee_api_' . $key . '_status', $form_state->getValue('integration_chargebee_api_' . $key . '_status'))

          ->save();
      }
    }

    $this->config('integration_chargebee.settings')
      ->set('integration_chargebee_webhook_hash', $form_state->getValue('integration_chargebee_webhook_hash'))
      ->set('integration_chargebee_api_site_name', $form_state->getValue('integration_chargebee_api_site_name'))
      ->set('integration_chargebee_api_key', $form_state->getValue('integration_chargebee_api_key'))
      ->set('integration_chargebee_api_js_link', $form_state->getValue('integration_chargebee_api_js_link'))

      ->save();
  }

}
