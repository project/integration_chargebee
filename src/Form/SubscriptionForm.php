<?php

namespace Drupal\integration_chargebee\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use ChargeBee\ChargeBee\Environment;
use ChargeBee\ChargeBee\Models\Customer;
use ChargeBee\ChargeBee\Models\PaymentSource;
use ChargeBee\ChargeBee\Models\Subscription;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Builds the chargebee subscription form.
 */
class SubscriptionForm extends ConfigFormBase {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new Drupal\integration_chargebee\Form\SubscriptionForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'subscriptionform.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscriptionform';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
      '#default_value' => '',
    ];

    $form['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
      '#default_value' => '',
    ];
    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#default_value' => '',
    ];
    $form['billingAddress'] = [
      '#type' => 'details',
      '#title' => $this->t('Billing Address'),
    ];
    $form['billingAddress']['line1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address line 1'),
      '#default_value' => '',
    ];

    $form['billingAddress']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#default_value' => '',
    ];
    $form['billingAddress']['state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State'),
      '#default_value' => '',
    ];
    $form['billingAddress']['country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country'),
      '#default_value' => '',
    ];
    $form['billingAddress']['zip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('zip'),
      '#default_value' => '',
    ];
    $form['payment_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Payment Information'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('integration_chargebee.settings');

    $site_name = $config->get('integration_chargebee_api_site_name');
    $site_api_key = $config->get('integration_chargebee_api_key');
    Environment::configure("$site_name", "$site_api_key");
    // Add customer on integration_chargebee.
    $result = Customer::create([
      "firstName" => $form_state->getValue('first_name'),
      "lastName" => $form_state->getValue('last_name'),
      "email" => $form_state->getValue('email'),
      "billingAddress" => [
        "firstName" => $form_state->getValue('first_name'),
        "lastName" => $form_state->getValue('last_name'),
        "line1" => $form_state->getValue('line1'),
        "city" => $form_state->getValue('city'),
        "state" => $form_state->getValue('state'),
        "zip" => $form_state->getValue('zip'),
        "country" => $form_state->getValue('country'),
      ],
    ]);

    $customer = $result->customer();
    $customer_id = $customer->id;
    // Added the card.
    $result = PaymentSource::createCard([
      "customerId" => $customer_id,
      "card" => [
        "number" => "378282246310005",
        "cvv" => "100",
        "expiryYear" => 2022,
        "expiryMonth" => 12,
      ],
    ]);

    // Subscribe the plan.
    $result = Subscription::createWithItems("$customer_id", [
      'invoiceImmediately' => TRUE,
      'termsToCharge' => 1,
      "subscriptionItems" => [[
        "itemPriceId" => "testbt-INR-Monthly",
        "unitPrice" => 10000,
      ],
      ],
    ]);
    $subscription = $result->subscription();
    $invoice = $result->invoice();
    // kint($subscription);
    // kint($invoice);
    // die;
  }

}
