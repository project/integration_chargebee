<?php

namespace Drupal\integration_chargebee\Form;

use ChargeBee\ChargeBee\Environment;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use ChargeBee\ChargeBee\Models\ItemPrice;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\integration_chargebee\Services\ChargebeeService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the chargebee plan form.
 */
class ChargebeePlanForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * The integration chargebee service.
   *
   * @var \Drupal\integration_chargebee\Services\ChargebeeService
   */
  protected $integrationChargebeeservice;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new Drupal\integration_chargebee\Form\ChargebeePlanForm object.
   *
   * @param \Drupal\integration_chargebee\Services\ChargebeeService $chargebeeService
   *   The integration chargebee service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ChargebeeService $chargebeeService, ConfigFactoryInterface $config_factory) {
    $this->integrationChargebeeservice = $chargebeeService;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('integration_chargebee.integration_chargebee_service'),
      $container->get('config.factory')
     );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'integration_chargebee.plan',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'integration_chargebeePlan_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('integration_chargebee.settings');
    $site_name = $config->get('integration_chargebee_api_site_name');
    $site_api_key = $config->get('integration_chargebee_api_key');
    if ($site_name != '' && $site_api_key != '') {
      Environment::configure("$site_name", "$site_api_key");
      $plan_list = ItemPrice::all([
        "limit" => 20,
        "orderby" => 'desc',
      ]);

      $form['plans'] = [
        '#type' => 'details',
        '#title' => $this->t('Select the plans that you want to replace'),
        '#tree' => FALSE,
      ];

      $form['plans']['list'] = $this->integrationChargebeeCreatePlansSelectTable($plan_list);
      $form['action'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),

      ];
    }
    else {
      $form['setup-chargebee'] = [
        '#type' => 'markup',
        '#markup' => $this->t('You need to setup chargebee first to access subscription plans. <br/> <a href="@administer-chargebee">Click here</a> to setup chargeBee.', ['@administer-chargebee' => '/admin/config/system/chargebee']),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('list') as $key => $value) {
      if ($value > 0) {
        $this->config('integration_chargebee.plan')
          ->set($key, $value)
          ->save();
      }
    }
  }

  /**
   * Create tableselect for plan list.
   */
  public function integrationChargebeeCreatePlansSelectTable($plans) {
    $config = $this->configFactory->get('integration_chargebee.plan');

    $select_data = array_keys($config->get());
    $header = [
      'plan_name' => $this->t('Plan Name'),
      'name' => $this->t('Name'),
      'price' => $this->t('Price'),
      'type' => $this->t('Type'),
      'status' => $this->t('Status'),
    ];
    $rows = [];

    foreach ($plans as $plan) {
      $item = $this->integrationChargebeeservice->getProtectedValue($plan->itemPrice(), '_data');
      if ($item['itemType'] == 'plan') {
        $rows[$item['id']] = [
          'plan_name' => $item['itemId'],
          'name' => $item['name'],
          'price' => $item['currencyCode'] . $item['price'] / 100,
          'type' => $item['itemType'],
          'status' => $item['status'],
        ];
      }
      $table = [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows,
        '#default_value' => $select_data,
        '#empty' => $this->t('No plans found'),
      ];
    }
    return $table;
  }

}
