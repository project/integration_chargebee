# ChargeBee

Chargebee module is providing plans with integrated plan page and block.
Allow your site visitors to sign up as customers on your Chargebee site and
easily manage all card payments using the Hosted Checkout integration

For a full description of the module, visit the
[project page](https://www.drupal.org/project/integration_chargebee).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/integration_chargebee).

## Table of contents

- Installation
- Configuration
- Maintainers

## Installation

Install the ChargeBee module as you would normally install a contributed
Drupal module.
Visit <https://www.drupal.org/node/1897420/> for further information.

## Configuration

1. Navigate to Administration > Extend and enable the module.
1. Go to Administation > Configuration > System > Chargebee
   Path: admin/config/system/chargebee
1. Add chargebee site name and api key :
   <https://www.chargebee.com/docs/2.0/api_keys.html/>
1. Choose the Plan from the Chargebee plan page.
   Path: admin/config/system/chargebee/plans
1. Now, user revisit on the chargebee setting page and enable the plan
   from the chargebee setting page. Path: admin/config/system/chargebee
1. Now, integrate the subscribe plan page anywehre on the site.
   So, user will access the page. Path: /subscribe-plan

## Maintainers

- Shashank Kumar - [shashank5563](https://www.drupal.org/u/shashank5563)
